package com.example.horoscope_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    lateinit var allHoroscopeDates:ArrayList<HoroscopesData>
// lateinit anahtari sayesinde allHoroscopeDates ilerleyen zamanlarda atama yapma imkani saglar zorunlu ilk atama yaptirmz

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        prepareSource()




      //  var myAdapter=ArrayAdapter<String>(this,R.layout.only_line,R.id.txtViewHoroscope,horoscopeB)
       // horoscopeList.adapter=myAdapter oyle

        //kendi olusturdugumuz adapter
      /* var myAdapter=HoroscopeArrayAdapter(this,R.layout.only_line,R.id.txtViewHoroscope,horoscopeB,horoscopeBDate,horoscopeBSymbol)
            horoscopeList.adapter=myAdapter */


        //baseAdapter

        var myBaseAdapter=HoroscopeBaseAdapter(this,allHoroscopeDates) //mainactivity contex gonderiyoruz
        horoscopeList.adapter=myBaseAdapter

        //amacimiz lsiteden herhangibir eleman tiklandiginda yeni bir activite olusturup tiklanan oge ile ilgili bilgileri orda kullanalim

        horoscopeList.setOnItemClickListener { parent, view, position, id ->
            //bu kisimda contex this diye gonderemiyoruz. ananim inner class oldugu ici direk this diye gelmiyor this@MainActivity boyle cagirmamiz gerekiyor

            var intent=Intent(this@MainActivity,DetailActivity::class.java)
            intent.putExtra("PositionData",position) //o an tikladigimiz view posisyonu
            intent.putExtra("allHoroscope",allHoroscopeDates)
            startActivity(intent)

        }


    }

    private fun prepareSource() {

        allHoroscopeDates= ArrayList<HoroscopesData>()
        //veri kaynağı olusturma islemini main activitede yapalim gerekli olan yerleri burdan yollayaim
        //resources erismek icin onCreate tanimlanmasi  gerek
        var horoscopeB=resources.getStringArray(R.array.horoscopeArr) //12 tane emin ol (2 side esit olmali yoksa java.lang.ArrayIndexOutOfBoundsException: length=11; index=11 at com.example.horoscope_app.horoscopeArrayAdapter.getView alirsin )
        var horoscopeBDate=resources.getStringArray(R.array.horoscopeDate) //12 tane emin ol

        var horoscopeBSymbol= arrayOf(R.drawable.koc1,R.drawable.boga2,R.drawable.ikizler3,R.drawable.yengec4,R.drawable.aslan5,R.drawable.basak6,
            R.drawable.terazi7,R.drawable.akrep8,R.drawable.yay9,R.drawable.oglak10,R.drawable.kova11,R.drawable.balik12)//12 tane emin ol


        var horoscopeBuyukSymbol= arrayOf(R.drawable.koc_buyuk1,R.drawable.boga_buyuk2,R.drawable.ikizler_buyuk3,R.drawable.yengec_buyuk4,R.drawable.aslan_buyuk5,R.drawable.basak_buyuk6,
            R.drawable.terazi_buyuk7,R.drawable.akrep_buyuk8,R.drawable.yay_buyuk9,R.drawable.oglak_buyuk10,R.drawable.kova_buyuk11,R.drawable.balik_buyuk12)//12 tane emin ol

        var generalFeaturesHoroscope=resources.getStringArray(R.array.generalFeatures) //values deki arrays icindeki generalFeatures string kisimini aldik


        for(i in 0..11){
            var tempData=HoroscopesData(horoscopeB[i],horoscopeBDate[i],horoscopeBSymbol[i],horoscopeBuyukSymbol[i],generalFeaturesHoroscope[i])
            allHoroscopeDates.add(tempData)
        }
    }
}
