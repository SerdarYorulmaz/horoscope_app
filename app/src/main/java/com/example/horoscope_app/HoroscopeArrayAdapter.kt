package com.example.horoscope_app

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.only_line.view.*

//  kendi olsuturdugumz adapter sinifimiz
class HoroscopeArrayAdapter(var giveContext: Context, resource: Int, textViewResourceId: Int, var horoscopeNames: Array<String>, var horoscopeDates: Array<String>, var imgHoroscope: Array<Int>
) : ArrayAdapter<String>(giveContext, resource, textViewResourceId, horoscopeNames) {

    //liste elemanları olusutururken tektiklenen method ne kadar satir varsa o kadar calisir

    //listenin o anki indis numarasi(satir olsuturulmadan once getView tetiklenir)
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        /*
        ConverView olsuturudugum yapıları tekrar tekrar kulnmaya saglayan yapı
        position: Int --> o an olusturulan view in idsi,parent si  listview dir

         */

        var viewHolderA:ViewHolder?=null

          var only_line_view = convertView?.let { convertView ->
            convertView
        } ?: kotlin.run {
            var inflater = LayoutInflater.from(giveContext)

            //amacimiz dinamik olarak view oluaturma

            Log.e("DENEME", "INFLATION YAPILDI: " + horoscopeNames[position])
            inflater.inflate(R.layout.only_line, parent, false) //amacimiz LAYOUT PARAMS almak(view grubunuzun genislik,yukseklik.. verilerinin tutugu siniftir.)



        }

        viewHolderA= ViewHolder(only_line_view)

        viewHolderA.imgHorScorpion.setImageResource(imgHoroscope[position])
        viewHolderA.txtViewHoroscope.setText(horoscopeNames[position])
        viewHolderA.txtHoroscopeDate.setText(horoscopeDates[position])
        //Layout ayri degerler(altdeki 3 kisimda sayilari esit olmali)
//        var horoscopeImageView = only_line_view?.imgHorScorpion
//        var horoscopeName = only_line_view?.txtViewHoroscope
//        var horoscopeDate = only_line_view?.txtHoroscopeDate
//
//        horoscopeImageView?.setImageResource(imgHoroscope[position]) //posiyonlarina gore sira sira ekliyor
//        horoscopeName?.setText(horoscopeNames[position])
//        horoscopeDate?.setText(horoscopeDates[position])

        Log.e("TEST", "" + parent?.id.toString())

        return only_line_view
    }

    class ViewHolder(only_line_viewA:View){
        var imgHorScorpion:ImageView
        var txtViewHoroscope:TextView
        var txtHoroscopeDate:TextView

        init {
            this.imgHorScorpion=only_line_viewA.imgHorScorpion
            this.txtHoroscopeDate=only_line_viewA.txtHoroscopeDate
            this.txtViewHoroscope=only_line_viewA.txtViewHoroscope
        }


    }
}