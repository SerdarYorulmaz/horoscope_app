package com.example.horoscope_app

import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.palette.graphics.Palette
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        //TODO 12CAL04:16

        var positionDataA=intent.extras?.get("PositionData") as Int
        var allData=intent.extras?.get("allHoroscope") as ArrayList<HoroscopesData> //type casting yapiyoruz(serilazmden arraylist cevirdik)

        txtHoroscopeFuture.setText(allData[positionDataA].generalFeaturesHoroscope)
        header.setImageResource(allData[positionDataA].bighoroscopeSymbol)

        setSupportActionBar(anim_toolbar)
        getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true)

        collapsing_toolbar.title=allData[positionDataA].horoscopeName //deait activity baslik koyduk

        var bitmap=BitmapFactory.decodeResource(resources,allData[positionDataA].bighoroscopeSymbol)
        Palette.from(bitmap).generate(object :Palette.PaletteAsyncListener{//anonim class yaptik
            override fun onGenerated(palette: Palette?) {
               var color=palette?.getVibrantColor(R.attr.colorAccent)

            collapsing_toolbar.setContentScrimColor(color?:R.attr.colorPrimary)

            window.statusBarColor= (color?:R.attr.colorPrimary)

            }

        })


        Toast.makeText(this,"Tikladiginin ogenin position:"+positionDataA+"Tum Burc sayisi:"+allData.size,Toast.LENGTH_LONG).show()


    }

    override fun onSupportNavigateUp(): Boolean {

        onBackPressed() //seni acan activity geri don
        return super.onSupportNavigateUp()
    }
}
